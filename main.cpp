#include <iostream>
//#include <vector>
#include <algorithm>
#include <math.h>
//#include <ctime>
//#include <cstring>
#include <random>
#include <vector>
#include <unordered_map>

//#include <bitset>
#include "sys/types.h"
#include "sys/sysinfo.h"
#include <stdio.h>
#include <string.h>

#include <iomanip> // del

//#include "pathfinders.h"

//#include <sstream>
#include "jps.hh"


//#include "astar.hpp"
//#include <cstdlib>

//extern "C" {
//#include "AStar.h"
//}

#include <chrono>



using namespace std;

using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::duration;
using std::chrono::milliseconds;

class TimeStat
{
    typedef std::chrono::high_resolution_clock::time_point TimeVar;
    typedef std::chrono::high_resolution_clock::duration TimeDur;
    typedef std::chrono::duration<unsigned long long, std::pico> picoseconds;
    typedef std::chrono::duration<unsigned long long, std::nano> nanoseconds;

    TimeVar t1;
    TimeDur min = TimeDur::zero();
    TimeDur max = TimeDur::zero();
    TimeDur total = TimeDur::zero();
    unsigned long long cnt = 0;

public:
    TimeStat(){
    }

    void start(){
       t1 = high_resolution_clock::now();
    }

    void stop(){
        TimeDur diff = high_resolution_clock::now() - t1;
        total += diff;
        if(cnt == 0) min = max = diff;
        if(diff<min or min == TimeDur::zero())
            min = diff;
        else if(diff>max)
            max = diff;
        cnt++;
    }

    void end(){
        cerr << "ticks:"
             << " min=" << min.count()
             << " max=" << max.count()
             << " avg=" << total.count() / long((cnt == 0) ? 1 : cnt)
             << " tot=" << total.count()
             << endl;

        cerr << "pico:"
             << " min=" << chrono::duration_cast< picoseconds >(min).count()
             << " max=" << chrono::duration_cast< picoseconds >(max).count()
             << " avg=" << chrono::duration_cast< picoseconds >(total).count() / ((cnt == 0) ? 1 : cnt)
             << " tot=" << chrono::duration_cast< picoseconds >(total).count()
             << " cnt=" << cnt
             << endl;
    }
};


struct Grid
{
    unsigned width, height;
    unsigned pMapSize, padding;
    unsigned char* pMap;

    Grid(unsigned char* pMap, unsigned int pMapSize, unsigned width, unsigned height):
        width{width},
        height{height},
        pMapSize{pMapSize},
        pMap{pMap}
    {

    }

    inline bool operator()(unsigned x, unsigned y) const // coordinates must be unsigned; method must be const
    {
        if(x < width && y < height) // Unsigned will wrap if < 0
        {
            if(pMap[(y*height)+x] == 1)
            {
                return true;
            }
        }
        return false;
    }
};

enum State
{
  N_MAXTIPS_COST,
  MAP,
  NUM_ITER_ZAKAZOV,
  NUM_ROBOTS,
  ROBOTS_PLACEMENT,
  ZAKAZOV_IN_THIS_DAY,
  ZAKAZ,
  COMPUTE_JOBS,
  PRINT_MOVEMENTS
};

class Rover
{
public:
    Rover(unsigned num, JPS::Position pos):
        pos(pos),
        num(num)
    {
        clearPathAndAddStartPos();
    }

    ~Rover()
    {
        get_order_points.clear();
        put_order_points.clear();
    }
    void clearPathAndAddStartPos()
    {
        if(path!=nullptr){
            path->clear();
        }
    }
    vector<unsigned> get_order_points;
    vector<unsigned> put_order_points;
    JPS::PathVector* path;
    JPS::Position pos;
    unsigned busy = false;
    unsigned num;
};

class Direction{
public:
    JPS::Position from;
    JPS::Position to;
    bool operator==(const Direction &other) const
    { return (from == other.from
              && to == other.to);
    }
};

namespace std {

template <>
struct hash<Direction>
{
  uint64_t operator()(const Direction& k) const
  {
    return     (uint64_t(k.from.x)
             + (uint64_t(k.from.y)  << 16)
             + (uint64_t(k.to.x)    << 16*2)
             + (uint64_t(k.to.y)    << 16*3));
  }
};
}

class Order
{
public:
    Direction direction;
    int num;
    short occuped = false;
    short finished = false;

    Order(int num, Direction dir):
        direction(dir),
        num(num)
        {}
};

class Job
{
public:
    Order* order;
    Rover* rover;
    int distance;
    int distance1;

    Job(Order* order, Rover* rover):
        order(order),
        rover(rover)
    {
        distance = abs(int(order->direction.from.x - rover->pos.x))
                 + abs(int(order->direction.from.y - rover->pos.y))
                 + abs(int(order->direction.from.x - order->direction.to.x))
                 + abs(int(order->direction.from.y - order->direction.to.y));
    }

    static bool compareByDistance(const Job& a, const Job& b)
    {
        return a.distance < b.distance;
    }

    static bool compareByPath(const Job& a, const Job& b)
    {
        return a.rover->path->size() < b.rover->path->size();
    }

    static bool compareByRover(const Job& a, const Job& b)
    {
        return a.rover->num < b.rover->num;
    }

    bool operator<(const Job& a) const
    {
        return order->num < a.order->num;
    }

    bool operator==(const Job& a) const
    {
        return order->num == a.order->num;
    }
};

class Planner
{
public:
  TimeStat plan_timer1;
  TimeStat plan_timer2;

  unordered_map<Direction, JPS::PathVector*> cache;
  Order empty_order;
  vector<Rover> rovers;
  vector<Order> orders;
  vector<Job> jobs_finished;
  vector<Job> jobs;
  vector<JPS::Position> freePlaces;


  Grid* grid;
  unsigned char *pMap;


  unsigned int* pOutBuffer;


  unsigned int pBufferSize;
  unsigned int line_num = 0;
  unsigned int n, max_tips, robot_cost,
    days, zakazov_total;
  int unsigned nStartRow, nStartCol, nTargetRow, nTargetCol;
  unsigned robots_total = 1;
  unsigned int avarage_path_len;
  unsigned int zakazov, zakaz_num;
  unsigned total_path_len = 0;
  unsigned debug = false;

  JPS::SizeT ret;

  State state = N_MAXTIPS_COST;

  unsigned int padding[1];

  Planner():
      empty_order({-1, {{0,0}, {0,0}}})
  {
      empty_order.occuped = true;
  }

  ~Planner()
  {
    delete[] pMap;
    delete  grid;

    for(auto &r: rovers){
        delete r.path;
    }
  }

  JPS::SizeT plan(JPS::PathVector& path, Direction dir)
  {
      JPS::Position b;
      b.x = dir.from.x;
      b.y = dir.from.y;
      JPS::PathVector* path_section;

      if(path.size() == 0)
      {
          path.push_back({dir.from.x, dir.from.y});
      }

      auto got = cache.find (dir);

      if(got == cache.end())
      {
          plan_timer1.start();
          path_section = new JPS::PathVector();
          JPS::PathVector* path_tmp = new JPS::PathVector();
          JPS::Position t;

          unsigned ret = JPS::findPath(*path_tmp, *grid, dir.from.x, dir.from.y, dir.to.x, dir.to.y, 1);
          if(!ret)
          {
              cerr << "not found!!" << endl;
              return 1;
          }

          // super ugly hack
          // JPS returns path with diagonals, we need to
          // convert it to non-diagonals path by adding points
          signed long diff_x, diff_y;
          for(auto &c : *path_tmp)
          {
              /*
              if(b == c){
                  // start pos?
                  continue;
              } */

              diff_x = int(b.x - c.x);
              diff_y = int(b.y - c.y);

              // is diagonal
              if( abs(int(diff_x)) == 1 and abs(int(diff_y)) == 1)
              {
                  t.x = unsigned(b.x - diff_x);
                  t.y = b.y;

                  if( pMap[(t.y*n)+(t.x)] == true) // ячейка справа/слева свобода
                    path_section->push_back(t);
                  else {
                      t.x = b.x;
                      t.y = unsigned(b.y - diff_y);
                      if( pMap[(t.y*n)+(t.x)] == true) // ячейка сверху/снизу свободна
                      {
                          path_section->push_back(t);
                      }
                      else {
                          cout << "cant find non diagonal path" << endl;
                          exit(1);
                      }
                  }
              }
              b = c;
              path_section->push_back(b);
          }
          // всегда включаем последнюю точку дважды (забор и выдача заказа)
          path_section->push_back(b);
          // очистка
          delete path_tmp;
          // кэширование
          cache[dir] = path_section;
          plan_timer1.stop();
      }
      else {
          plan_timer2.start();
          path_section = got->second;
          plan_timer2.stop();
      }

      auto old_cnt = path.size();
      path.resize(path.size() + path_section->size());
      memcpy(path.begin()+old_cnt,
             path_section->begin(),
             path_section->size()*sizeof (JPS::Position));

      return path_section->size();
  }

  unsigned findNearest()
  {
      vector<Job> jobs_variants;
      unsigned new_jobs = 0;
      unsigned not_busy = 0;

      /*if(orders.size()==0)
          return 0;*/

      // сохраняем все сочетания свободных роверов и заказов в отдельный массив
      for (auto &rover: rovers)
          if(not rover.busy){
              not_busy++;
              for (auto &order: orders)
                  if(not order.occuped)
                      jobs_variants.push_back({&order, &rover});
          }
      // сортируем список вариантов по дистанции
      sort(jobs_variants.begin(), jobs_variants.end(), Job::compareByDistance);

      // находим роверы, которые находятся ближе всего к заказам
      while(jobs_variants.size()>0 and not_busy>0){
          // сохраняем первую Joбу (она ближе всего)
          Job fastest_job = jobs_variants.front();
          fastest_job.rover->busy = true;
          jobs.push_back(fastest_job);
          new_jobs++;
          not_busy--;
          if(not_busy == 1)
              cout << jobs.back().rover->num << endl;
          if(not_busy == 0)
              break;

          // удаляем все Joбы, у которых тот же order.num (они дальше)
          //*
          for (auto it=jobs_variants.begin();it!=jobs_variants.end();) {
              if(it->rover->num == fastest_job.rover->num
              or it->order->num == fastest_job.order->num){
                  jobs_variants.erase(it);
              } else {
                  it++;
              }
          } //*/
      }
      return new_jobs;
  }

  // жуём ввод
  int chew(istream& ss)
  {
    switch (state)
    {
    case N_MAXTIPS_COST:
      ss >> n >> max_tips >> robot_cost;
      if(debug){
        cerr << "> " << n << " " << max_tips << " " << robot_cost << endl;
      }
      pBufferSize = n * n;
      pMap = new unsigned char[pBufferSize];
      line_num = 0;
      state = MAP;
      break;

    case MAP: {
      string line;
      for (unsigned int y=0; y<n;y++) {
          ss >> line;
          for(unsigned x=0; x<line.length(); x++)
          {
              if(line[x] == '#'){
                  pMap[(y*n)+x] = 0;

              }
              else {
                  pMap[(y*n)+x] = 1;
                  freePlaces.push_back({x,y});
              }
          }
      }

      grid = new Grid(pMap, pBufferSize, n, n);
      state = NUM_ITER_ZAKAZOV;
      break;
    }
    case NUM_ITER_ZAKAZOV:
      ss >> days >> zakazov_total;
      if(debug){
        cerr << "> " << days << " " << zakazov_total << endl;
      }
      state = NUM_ROBOTS;
      break;

    case NUM_ROBOTS:{
      //JPS::Position p = ; //, freePlaces.end()
      //Mission m();
      // compute longest path from the left upper conner to the right bottom conner
      // максимальная длина пути (не пригодилась)
      /*
      JPS::PathVector* path = new JPS::PathVector();
      plan(*path, {freePlaces.front(), freePlaces.back()});
      showPath(*path);
      plan(*path, {freePlaces.front(), freePlaces.back()});
      showPath(*path);
      delete path; */
      //avarage_path_len = path->size() / 2;

#define KOEFF1 1.0
#define KOEFF2 10.0
      // тут я пытался счтитать оптимальное количество роботов
      float dz = float(zakazov_total)/float(days);
      float f = float( (float(KOEFF1) * (n * (dz) )) / (robot_cost) );

      if(debug){
          cerr
               << n << " "
               //<< avarage_path_len << " "
               << robot_cost << " "
               << max_tips << " "
               << zakazov_total << " "
               << days << " dz="
               << int(round(dz)) << " "
               << fixed << setprecision(3)
               << dz << " "
               << f << endl;
      }

      // грубый расчет в зависимости от величины карты
      switch (n) {
      case 4:
        robots_total = 2;
        break;
      case 128:
        robots_total = 2;
        break;
      case 180:
        robots_total = 2;
        break;
      case 384:
        robots_total = 3;
        break;
      case 1024:
        robots_total = 4;
        break;
      case 1000:
        robots_total = 5;
        break;
      default:
        robots_total = 1;
        break;
      }

      // для тестирования пока ограничил количество роботов до 1
      robots_total = 1;

      cout << robots_total << endl;

      state = ROBOTS_PLACEMENT;
      break;
    }

    case ROBOTS_PLACEMENT:{
      std::random_device rd;  //Will be used to obtain a seed for the random number engine
      std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
      std::uniform_int_distribution<unsigned long> distrib(0, freePlaces.size());
      for (unsigned i=0;i<robots_total;i++) {
          JPS::Position p = freePlaces[distrib(gen)];// freePlaces[distrib(gen)]; //freePlaces[freePlaces.size()/2];
          Order o(short(i), {p, p});
          Rover r(i, p);
          r.path = new JPS::PathVector();
          rovers.push_back(r);
          cout << (p.y+1) << " " << (p.x+1) << endl;
      }
      state = ZAKAZOV_IN_THIS_DAY;
      break;
    }

    case ZAKAZOV_IN_THIS_DAY:
      ss >> zakazov;
      zakaz_num = 0;

      if(debug){
        cerr << "> " << zakazov << endl;
      }
      if(zakazov>0){
          state = ZAKAZ;
      } else {
          // быстрый хак (неравильно понял задание, если пришло 0 заказов в этот день то нужно стоять?)
          //*
          for (unsigned i=0;i<robots_total;i++) {
            cout << "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS" << endl;
          } //*/
      }
      break;

    case ZAKAZ:{
      ss >> nStartRow >> nStartCol >> nTargetRow >> nTargetCol;
      if(debug){
        cerr << "> " << nStartRow << " " << nStartCol << " " << nTargetRow << " " << nTargetCol << endl;
      }
      nStartRow--; nStartCol--; nTargetRow--; nTargetCol--;
      //Order o({zakaz_num, {{nStartCol, nStartRow}, {nTargetCol, nTargetRow}}});
      orders.push_back({int(zakaz_num), {{nStartCol, nStartRow}, {nTargetCol, nTargetRow}}});

      zakaz_num++;
      if(zakaz_num >= zakazov)
          state = COMPUTE_JOBS;

      break;
    }

    case COMPUTE_JOBS:
      {
        while (findNearest() > 0){
          for (auto& job: jobs) {
            if(job.order->occuped)
                continue;


            if(debug){
                cerr << "rover=" << job.rover->num << " order=" << job.order->num << endl;
                cerr << "(" << job.rover->pos.y+1 << " " << job.rover->pos.x+1 << ") -> ";
                cerr << "(" << job.order->direction.from.y+1 << " " << job.order->direction.from.x+1 << ") -> ";
                cerr << "(" << job.order->direction.to.y+1 << " " << job.order->direction.to.x+1 << ")" << endl;
            }

            ret = 0;

            job.order->occuped = true;
            // сначала нужно приехать забрать заказ
            ret += plan(*job.rover->path, {job.rover->pos, job.order->direction.from} );
            // добавим точку откуда нужно забрать заказ в список get
            unsigned a = job.rover->path->size()-1;
            job.rover->get_order_points.push_back(a);
            showPath(*job.rover);

            if(debug)
              cerr << "plan path len = " << ret << endl;
            job.rover->pos = job.order->direction.from;
            // теперь отвезем заказ
            ret += plan(*job.rover->path, {job.rover->pos, job.order->direction.to} );
            // добавим точку куда нужно доставить заказ в список put
            a = job.rover->path->size()-1;
            job.rover->put_order_points.push_back(a);
            showPath(*job.rover);

            if(debug)
              cerr << "plan path len = " << ret << endl;
            job.rover->pos = job.order->direction.to;
          }
          // отсортируем все задачи по длине пути
          sort(jobs.begin(), jobs.end(), Job::compareByPath);
          Job shotest = jobs.front();
          // эти роверы готовы принять новый заказ
          // на всякий случай просканируем остальные,
          // вдруг какие-то задачи закончились одновременно
          for (auto& job: jobs) {
              if(job.rover->path->size() != shotest.rover->path->size())
                  break;
              // освобождаем ровер
              job.rover->busy = false;
          }
          // скопируем jobs в конец jobs_finished
          //jobs_finished.insert(jobs_finished.end(), jobs.begin(), jobs.end());
          //jobs.clear();
        }

        //unsigned new_jobs = ;

        //if(findNearest()==0)
        state = PRINT_MOVEMENTS;

        break;
      }
    case PRINT_MOVEMENTS:
      {
        if(debug){
            cerr << "===============================" << endl;
            cerr << "===============================" << endl;
            cerr << "===============================" << endl;
        }
        sort(jobs.begin(), jobs.end(), Job::compareByPath);
        Job longest_job = jobs.back();
        unsigned longest = longest_job.rover->path->size();
        unsigned ost = 60 - (longest % 60);
        longest += ost+1;
        // expand standing rovers
        for (auto& rover: rovers) {
            unsigned old_size = rover.path->size();
            rover.path->resize(longest);
            auto last_pos = rover.path->begin()+old_size;
            fill_n(last_pos,
                   (longest-old_size),
                  *(last_pos-1));
        }
        unsigned k=0;
        for (unsigned i=1;i<longest;i++)
        {
            for (auto& rover: rovers)
            {
                if(k>0 and k % 60==0)
                    cout << '\n';
                JPS::Position* pos_cur = &rover.path->data()[i];
                JPS::Position* pos_pre = pos_cur-1;
                if(*pos_cur == *pos_pre )
                {
                    auto f = find(rover.get_order_points.begin(), rover.get_order_points.end(), i);
                    if (f != rover.get_order_points.end()) {
                        cout << "T"; k++;
                        continue;
                    }
                    f = find(rover.put_order_points.begin(), rover.put_order_points.end(), i);
                    if (f != rover.put_order_points.end()) {
                        cout << "P"; k++;
                        continue;
                    }
                    cout << "S"; k++;
                    continue;
                }
                if(pos_cur->x < pos_pre->x)
                {
                    cout << "L"; k++;
                    continue;
                }
                if(pos_cur->x > pos_pre->x)
                {
                    cout << "R"; k++;
                    continue;
                }
                if(pos_cur->y > pos_pre->y)
                {
                    cout << "D"; k++;
                    continue;
                }
                if(pos_cur->y < pos_pre->y)
                {
                    cout << "U"; k++;
                    continue;
                }
            }
        }
        cout << endl;

        for (auto& rover: rovers) {
            showPath(rover);
            //if(rover.path->size() > max)
            //    max = rover.path->size();
            rover.clearPathAndAddStartPos();
            rover.path->dealloc();
            rover.busy = false;
            rover.get_order_points.clear();
            rover.put_order_points.clear();
        }
        total_path_len += longest;
        orders.clear();
        jobs.clear();
        state = ZAKAZOV_IN_THIS_DAY;
        break;
      }
    }
    return 0;
  }

  void showPath(Rover& rover)
  {
      if (not debug)
          return;

      vector<unsigned int> pathBuffer;
      unsigned outLen=0;
      for(auto &k : *rover.path)
      {
          pathBuffer.push_back((k.y*n)+k.x);
          outLen++;
      }

      unsigned x = 0;
      unsigned y = 0;
      size_t show_x_coord = 0;

      for(uint32_t i=0; i<pBufferSize; i++, x++)
      {
          if(i % n == 0)
          {
              x = 0;
              // Header
              if(i == 0){
                  for (size_t i = 0; i < n+1; i++)
                      cout << ((i % 10 == 0) ? std::to_string(i / 10).back() : '-');
                  cout << "+" << n << endl;
              }

              // Put a barrier on the left-hand side.
              cout << (( (y+1) % 10 == 0) ? '+' : '|');
          }

          JPS::Position p({x,y});
          auto f = find(rover.path->begin(), rover.path->end(), p);
          if (f != rover.path->end()) {
              if(debug and pMap[i] == false)
              {
                  // просто доп. проверка постфактум
                  cerr << "Нельзя сотворить здесь. Эта клетка занята!" << endl;
                  exit(1);
              }
              if(rover.path->front() == p){
                  cout << "S";
                  show_x_coord = (i % n)+1;
              }else{
                  /*
                  // это больше не работает
                  auto get = find(rover.get_order_points->begin(), rover.get_order_points->end(), i);
                  auto put = find(rover.put_order_points->begin(), rover.put_order_points->end(), i);
                  if(get != rover.get_order_points->end()){
                      cout << "G";
                      show_x_coord = (i % n)+1;
                  }else if(put != rover.put_order_points->end()){
                      cout << "P";
                      show_x_coord = (i % n)+1;
                  }else{
                      cout << '@';
                  } //*/
                  cout << '@';
             }
          }
          else
          {
              cout << ((pMap[i] == 1) ? "." : " ");
          }

          if((i != 0) and (i+1) % n == 0)
          {
              y++;
              // Put a barrier on the right-hand side.
              cout << '|' << y;
              if(show_x_coord>0)
                cout << 'x' << show_x_coord;
              show_x_coord = 0;
              cout << endl;
          }
      }
      // Footer
      for (size_t i = 0; i < n+1; i++)
        cout << ((i % 10 == 0) ? std::to_string(i / 10).back() : '-');
      cout << "+" << n << endl;
  }

  unsigned long long clearCache(){
      unsigned long long mem = 0;
      for (auto& c: cache) {
          mem += c.second->_getMemSize();
          delete c.second;
      }
      return mem;
  }
};

size_t parseLine(char* line){
    // This assumes that a digit will be found and the line ends in " Kb".
    size_t i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    return size_t(atoi(p));
}

size_t getVirtual(){ //Note: this value is in KB!
    FILE* file = fopen("/proc/self/status", "r");
    size_t result = 0;
    char line[128];

    while (fgets(line, 128, file) != nullptr){
        if (strncmp(line, "VmSize:", 7) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}

size_t getPhysical(){ //Note: this value is in KB!
    FILE* file = fopen("/proc/self/status", "r");
    size_t result = 0;
    char line[128];

    while (fgets(line, 128, file) != nullptr){
        if (strncmp(line, "VmRSS:", 6) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}

int main(int argc, char const *argv[])
{
  ios_base::sync_with_stdio(false);
  cin.tie(nullptr);
  Planner p;
  if (argc>1) {
      p.robots_total = unsigned(atoi(argv[1]));
  }

  p.debug = false;
  while (cin.good()) {
      int ret = p.chew(cin);
      if(ret != 0)
          return ret;
  }

  unsigned long long mem = p.clearCache();

  if(p.debug){
      cerr << "total_path_len=" << p.total_path_len << endl;

      cerr << endl << "not cached" << endl;

      p.plan_timer1.end();
      cerr << endl << "cached" << endl;
      p.plan_timer2.end();

      struct sysinfo memInfo;

      sysinfo (&memInfo);
      unsigned long long totalVirtualMem = memInfo.totalram;
      //Add other values in next statement to avoid int overflow on right hand side...
      totalVirtualMem += memInfo.totalswap;
      totalVirtualMem *= memInfo.mem_unit;

      unsigned long long virtualMemUsed = memInfo.totalram - memInfo.freeram;
      //Add other values in next statement to avoid int overflow on right hand side...
      virtualMemUsed += memInfo.totalswap - memInfo.freeswap;
      virtualMemUsed *= memInfo.mem_unit;

      size_t procVirtual = getVirtual();
      size_t procPhysical = getPhysical();

      cerr << "memory:"
           << " totalVirtualMem=" << totalVirtualMem / 1000000 << "MB"
           << " virtualMemUsed=" << virtualMemUsed / 1000000 << "MB"
           << " procVirtual=" << procVirtual  / 1000 << "MB"
           << " procPhysical=" << procPhysical / 1000 << "MB"
           << endl;

      cerr << "cache memory:"
           << " bytes=" << mem
           << " kb=" << mem / 1000
           << " mb=" << mem / 1000000
           << endl;

  }

  return 0;

}
